''' trabajo extaclase '''

lista_carros = list()


def registrar(placa, marca, modelo, color, ano):
    """
    Registra un nuevo carro sí no existe
    :param placa: El número de placa del carro (Números y letras)
    :param marca: La marca del carro
    :param modelo: El modelo del carro
    :param color: El color del carro
    :param ano: El año del carro (números)
    """
    nuevoCarro = {"placa": placa, "marca": marca, "modelo": modelo, "color": color, "año": ano}
    if lista_carros.count(nuevoCarro) == 0:
        lista_carros.append(nuevoCarro)
        print("\nCarro agregado con éxito")
        return
    else:
        print("\n Este carro ya existe ")
        return


def modificar(placa, marca, modelo, color, ano):
    """
     Modifica un carro sí ya existe
     :param placa: El número de placa del carro (Números y letras)
     :param marca: La nueva marca del carro
     :param modelo: El nuevo modelo del carro
     :param color: El nuevo color del carro
     :param ano: El nuevo año del carro (números)
     """
    for carro in lista_carros:
        if carro["placa"] == placa:
            carro["marca"] = marca
            carro["modelo"] = modelo
            carro["color"] = color
            carro["ano"] = ano
            print("Carro modificado con éxito")
            return

    print("Carro no registrado")


def imprimir():
    """
    Muestra todos los carros.

    """
    for carro in lista_carros:
        print(carro)


def buscar(atributo, valor):
    """
    Busca un carro según un atributo y su valor.
    :param atributo: Puede ser placa, marca, modelo, color o año.
    :param valor: El valor relacionado al atributo correspondiente.

    """
    for carro in lista_carros:
        if carro[atributo] == valor:
            print(carro)


menu = ('\nCarros UTN\n'
        '1.Agregar carros\n'
        '2.Modificar\n'
        '3.Imprimir\n'
        '4.Buscar\n'
        '5.Salir\n'
        'Seleccione una opcion: ')
while True:
    op = int(input(menu))

    if op == 1:
        print("\nRegistrando carro...")
        placa = input("Digite placa: ")
        marca = input("Digite marca: ")
        modelo = input("Digite modelo: ")
        color = input("Digite color: ")
        ano = input("Digite año: ")
        registrar(placa, marca, modelo, color, ano)

    elif op == 2:
        print("\nModificando carro...")
        placa = input("Digite placa: ")
        marca = input("Digite nueva marca: ")
        modelo = input("Digite nuevo modelo: ")
        color = input("Digite nuevo color: ")
        ano = input("Digite nuevo año: ")
        modificar(placa, marca, modelo, color, ano)

    elif op == 3:
        print("\nImprimiendo todos los carros")
        imprimir()

    elif op == 4:
        print("\nBuscando carro...")
        atributo = input("Digite atributo (placa, marca, modelo, color, año): ")
        valor = input("Digite valor de atributo: ")
        buscar(atributo, valor)

    elif op == 5:
        break
    else:
        print("\nOpción incorrecta")
