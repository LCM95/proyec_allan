from datos import *
from datetime import *

dicionario = {}
usuarios = {}
citas = {}


def login(id, contrasena):
    """
    Inicia sesión
    :param id:
    :param contrasena:
    :return:
    """
    for usuarion in listaUsuarios:
        if usuarion['id'] == id and usuarion['contrasena'] == contrasena:
            return usuarion
    return dict()


def registrar_usuarios(id, nombreC, fechaN, correo, tipoUsu, contrasena):
    """
    Registra usuarios
    :param id: Cedula
    :param nombreC: Nombre completo
    :param fechaN: Fecha nacimiento
    :param correo: coreo
    :param tipoUsu: tipo de usuario (doc/sec)
    :param contrasena: contraseña

    """
    nueUsuario = {'id': id, 'nombreC': nombreC, 'fechaN': fechaN, 'correo': correo, 'tipo': tipoUsu,
                  'contrasena': contrasena}
    if listaUsuarios.count(nueUsuario) == 0:
        listaUsuarios.append(nueUsuario)
        print('Usuario agregado ')
        return
    else:
        print('\n El usuario ya existe')


# MEDICO

def consultar_cita(fecha):
    """
    Consulta citas por fecha
    :return: Las citas encontradas
    """
    if len(fecha) == 0:
        fecha = (datetime.strftime(datetime.today(), '%d/%m/%Y'))

    texto = ''
    for cita in listaCitas:
        if True == cita['CitaPendiente'] and cita['fecha'] == fecha:
            texto += ('\n Citas para {0} '.format(fecha))
            texto += ('Paciente cédula {}'.format(cita['idPasiente'], cita['medico']))

    return texto


def registrar_citas(medico, fecha, idPasiente):
    """
    Registra una cita a un paciente
    :param medico: Nombre del medico
    :param fecha: Fecha de la cita
    :param idPasiente: Cédula del paciente
    :param pendiente: Atendidd o no
    :return: mensaje de confirmacion
    """
    nuevaCita = {'medico': medico, 'fecha': fecha, 'idPasiente': idPasiente, 'CitaPendiente': True}

    pendientes = 0
    for cita in listaCitas:
        if cita['idPasiente'] == idPasiente and cita['CitaPendiente'] == True:
            pendientes += 1
    if pendientes < 2:
        listaCitas.append(nuevaCita)
        return "Se ha registrado exitosamente!!"

    return "Este usuario ya cuenta con dos citas pendientes."


def antender_cita_paciente(idPasiente, fecha):
    """
    Verifica sí el paciente tiene cita y lo procesa
    :param idPasiente: Cédula de paciente
    :param fecha: Fecha de la cita

    """
    if len(fecha) == 0:
        fecha = (datetime.strftime(datetime.today(), '%d/%m/%Y'))

    for cita in listaCitas:
        if True == cita['CitaPendiente'] and cita['fecha'] == fecha and cita['idPasiente'] == idPasiente:
            cita.update({'CitaPendiente': False})
            return True
    return False


def recetar(idPasiente, fecha, diagnostico, receta, medico):
    """
    Recetas para pacientes sin cita
    :param idPasiente: Cédula de paciente
    :param diagnostico: El diagnostico o el motivo
    :param receta: El medicamento y la forma de tomarlo
    :param medico: Medico que receta
    :return: La receta medica.
    """
    if len(fecha) == 0:
        fecha = (datetime.strftime(datetime.today(), '%d/%m/%Y'))

    nuevoReceta = {'idPaciente': idPasiente, 'fecha': fecha, 'diagnostico': diagnostico,
                   'receta': receta, 'medico': medico}
    listaRecetas.append(nuevoReceta)

    texto = ('\nReceta Médica  Paciente: {0} Fecha: {1}\n'
             'Diagnostico {2} Médico: {3}\n'
             '{4}'.format(idPasiente, fecha, diagnostico, medico, receta))
    return texto


# secretaria

def imprimir_comprobante(idPasiente, fecha):
    """
    Imprime comprobantes para pacientes
    :param idPasiente: Cédula
    :param fecha: Fecha
    :return: comprobante de texto
    """
    texto = ''
    for cita in listaCitas:
        if cita['idPasiente'] == idPasiente and cita['fecha'] == fecha:
            texto += ('\nComprbante para el paciente Cédula: {0}\n'
                      'Con el doctor {1} en la fecha {2}\n'
                      'Pendiente = {3}'.format(cita['idPasiente'], cita['medico'], cita['fecha'],
                                               cita['CitaPendiente']))
    return texto


def registrar_paciente(id, nombre, apellido, sexo, fechaN, telefono, correo, direccion, estado, ):
    """
    Registra pacientes
    :param nombre: El nombre del paciente
    :param apellido: El apellido del paciente
    :param sexo: Sexo (M/F)
    :param fechaN: La fecha de nacimiento (dd/mm/aaaa)
    :param telefono: El telefono del paciente
    :param correo: El correo electronico del paciente
    :param direccion: La direccion del paciente
    :param estado: Estado civil del paciente
    :return: mensaje de confirmacion
    """
    nuePas = {'id': id, 'nombre': nombre, 'apellido': apellido, 'sexo': sexo, 'fechaN': fechaN, 'telefono': telefono,
              'conrreo': correo, 'direccion': direccion, 'estado': estado}
    if listaPacientes.count(nuePas) == 0:
        listaPacientes.append(nuePas)
        return 'Usuario agregado'
    else:
        return 'El usuario ya existe'


# PACIENTES

def consultar_paciente(cedula):
    """
    Consulta los datos de un paciente
    :param cedula: El número de cédula del paciente.
    :return: diccionario con datos del paciente.
    """
    for paciente in listaPacientes:
        if paciente['id'] == cedula:
            return paciente
    return dict()


def ultimas_tres_citas(cedula):
    """
    Consulta las ultimas citas de un paciente
    param cedula: El número de cédula del paciente
    :return: Las ultimas tres citas a mostrar
    """
    ultimas_citas = list()

    for cita in listaCitas:
        if cita['idPasiente'] == cedula and cita['CitaPendiente'] == False:
            citaM = {'medico': cita['medico'], 'fecha': datetime.strptime(cita['fecha'], '%d/%m/%Y'),
                     'idPasiente': cita['idPasiente'], 'CitaPendiente': False}
            ultimas_citas.append(citaM)

    texto = ''

    if len(ultimas_citas) > 0:
        ultimas_citas.sort(key=lambda cita: cita['fecha'])
        #print(ultimas_citas)

        for i in range(0, 3):
            cita = ultimas_citas.pop()
            texto += ('\n Cita con el doctor {0}. Fecha: {1}'.format(cita['medico'], cita['fecha']))
            if len(ultimas_citas) < 1:
                break
    return texto


def consultar_expediente(cedula):
    """
    Consulta el expediente de un paciente
    :param cedula: El número de cédula del paciente
    :return: Información del paciente
    """
    paciente = consultar_paciente(cedula)
    texto = ""
    if len(paciente) != 0:
        texto = ('\n Cedula: {0} Nombre {1} {2}'.format(paciente['id'], paciente['nombre'], paciente['apellido']))
        texto += ('\n Sexo: {0} Nacimiento: {1}'
                  ' Teléfono: {2} Correo: {3} Dirrección: {4} Estado: {5}'
                  ''.format(paciente['sexo'], paciente['fechaN'], paciente['telefono'], paciente['conrreo'],
                            paciente['direccion'], paciente['estado'])
                  )

        for diagnostico in listaRecetas:
            if diagnostico['idPaciente'] == cedula:
                texto += "\n\n Fecha: {0} Diagnostico: {1} Medico: {2}\n" \
                         " Receta: {3}".format(diagnostico['fecha'], diagnostico['diagnostico'], diagnostico['medico'],
                                               diagnostico['receta'])
    return texto


def citas_pendientes(cedula):
    """
    Consulta las citas pendientes del paciente.
    :param cedula: El número de cédula del paciente
    :return: Las citas pendientes a mostrar
    """

    texto = ""

    for cita in listaCitas:
        if cita['idPasiente'] == cedula and cita['CitaPendiente'] == True:
            texto = ('\n Cita pendiente con el doctor {0} para el día: {1}'.format(cita['medico'], cita['fecha']))

    return texto
