from funciones import *

menuLogin = ('\n Bienvenido al consultorio Médico UTN1.01 \n'
             ' 1) Login.\n'
             ' 2) Registro.\n'
             ' 3) consultar por cédula .\n'
             ' 4) Salir .\n'
             ' Seleccione una opción: ')


def menuMedi(usuario):
    """
    Comente
    :param usuario:
    :return:
    """
    menuMedi = ('\n Bienvenido Doctor(a) {0} .\n'
                '1. Consultar cita.\n'
                '2. Registrar cita.\n'
                '3. Atender paciente.\n'
                '4. Recetas.\n'
                '5. Cerrar sesion.\n'
                'Seleccione una opción: '.format(usuario['nombreC']))

    while True:
        op = int(input(menuMedi))
        if op == 1:
            print('Consultar citas')
            fecha = input('Introzuca fecha a consultar o deje en blanco para ver las de hoy. ')
            citas = consultar_cita(fecha)
            if len(citas) != 0:
                print(citas)
            else:
                print('No hay citas hoy')
        elif op == 2:
            print('\n Registrar cita ')
            medico = ('{0}'.format(usuario['nombreC']))
            fecha = input('Digite la fecha (dd/mm/aaaa): ')
            paciente = consultar_paciente(int(input('Digite la cedula de paciente: ')))
            if len(paciente) != 0:
                print(registrar_citas(medico, fecha, paciente['id']))
            else:
                print("Debe registrar primero al paciente")

        elif op == 3:
            print('\n Atender paciente')
            fecha = input('Introzuca fecha a consultar o deje en blanco para ver las de hoy. ')
            cita = consultar_cita(fecha)
            print(cita)
            if len(cita) != 0:
                cedula = int(input('Digite la cedula de paciente: '))
                if antender_cita_paciente(cedula, fecha) == True:
                    diagnostico = input('digite diagnostico ')
                    receta = input('Nombre del medicaento y forma de tomarlo')

                    print(recetar(cedula, fecha, diagnostico, receta, usuario['nombreC']))
                else:
                    print('Paciente no tiene cita hoy')
            else:
                print("No hay citas hoy")

        elif op == 4:
            print('\n Receta ')
            cedula = int(input('digite la cedula del paciente '))
            fecha = input('Introzuca fecha o deje en blanco para ver de hoy. ')
            diagnostico = input('digite diagnostico ')
            receta = input('Nombre del medicaento y forma de tomarlo')
            print(recetar(cedula, fecha, diagnostico, receta, usuario['nombreC']))

        elif op == 5:
            break
        else:
            print('\n Opcion incorecta.')


def menuSecr(usuario):
    """
    Comente
    :param usuario:
    :return:
    """
    menuSec = ('\n Bienvenido Secretario(a) {0}\n'
               '1. Registrar Citas. \n'
               '2. Imprimir comprobantes.\n'
               '3. Registrar pacientes.\n'
               '4. Cerrar sesion.\n'
               'Seleccione una opción: '.format(usuario['nombreC']))

    while True:
        op = int(input(menuSec))
        if op == 1:
            print('\n Registrar cita')
            medico = input('Digite nombre completo de medico ')
            fecha = input('Digite la fecha (dd/mm/aaaa): ')
            paciente = consultar_paciente(int(input('Digite la cedula de paciente: ')))
            if len(paciente) != 0:
                print(registrar_citas(medico, fecha, paciente['id']))
            else:
                print("Debe registrar primero al paciente")

        elif op == 2:
            print('\n Imprimir comprobante. ')
            cedula = int(input('Digite cedula paciente '))
            fecha = input('Digite fecha de la cita (dd/mm/aaaa)')
            print(imprimir_comprobante(cedula, fecha))


        elif op == 3:
            print('\n Registrar paciente.')
            id = int(input('Digite su cedula: '))
            nombre = input('Digite su nombre: ')
            apellido = input('Digite su apellido: ')
            sexo = input('Digite su sexo (M/F) ')
            fechaNa = input('Digite su fecha de nacimiento: ')
            telefono = int(input('Digite su numéro de telefono: '))
            correo = input('Digite su correo:')
            direccion = input('Digite su direccion: ')
            estado = input('¿Cuál es su estado civil: ')
            print(registrar_paciente(id, nombre, apellido, sexo, fechaNa, telefono, correo, direccion, estado))

        elif op == 4:
            break
        else:
            print('\n Opcion incorecta.')


def menuPaciente(paciente):
    """
    Muestra el menú para pacientes en pantalla
    :param paciente: Los datos de un paciente

    """

    menuPac = ('\n Bienvenido {0} {1} Cédula: {2}\n'
               '1. Últimas tres citas . \n'
               '2. Expediente médico.\n'
               '3. Citas pedientes.\n'
               '4. Cerrar.\n'
               'Seleccione una opción: '.format(paciente['nombre'], paciente['apellido'], paciente['id']))
    cedula = paciente['id']
    while True:

        op = int(input(menuPac))
        if op == 1:
            print('\n Ultimas tres citas')
            print(ultimas_tres_citas(cedula))

        elif op == 2:
            print('\n Expediente médico ')
            print(consultar_expediente(cedula))

        elif op == 3:
            print('\n Citas pendientes.')
            print(citas_pendientes(cedula))

        elif op == 4:
            break
        else:
            print('\n Opcion incorecta.')


while True:
    op = int(input(menuLogin))
    if op == 1:
        print('\n Iniciar sesion. ')
        id = int(input('Intruduzca su id (cédula): '))
        contrasena = int(input('Introduzca su contraseña: '))
        usuario = login(id, contrasena)
        if len(usuario) != 0:
            if usuario['tipo'] == 'doc':
                menuMedi(usuario)
            elif usuario['tipo'] == 'sec':
                menuSecr(usuario)
        else:
            print('Usuario o contraseña incorecta. ')
    elif op == 2:
        print("\n Regisitrando Usuario...")
        id = input("Introduzca su id (cédula): ")
        nombreC = input("Introduzca su nombre completo: ")
        fechaN = input("Introduzca su fecha de nacimiento: ")
        correo = input("Introduzca su correo electronico: ")
        tipo = input("Introduzca su tipo de usuario (doc, sec): ")
        contrasena = input("Introduzca su contraseña: ")

        registrar_usuarios(id, nombreC, fechaN, correo, tipo, contrasena)
    elif op == 3:
        cedula = int(input("Introduzca el número de cédula del paciente"))
        paciente = consultar_paciente(cedula)
        if len(paciente) != 0:
            menuPaciente(paciente)
        else:
            print("Paciente no registrado")
    elif op == 4:
        print("\n Saliendo del programa...")
        break
    else:
        print("\n ¡Opción incorrecta!")
